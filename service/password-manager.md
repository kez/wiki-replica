A password manager is a service that securely stores multiple
passwords without the user having to remember them all. TPA uses
[password-store](https://www.passwordstore.org/) to keep its secrets, and this page aims at
documenting how that works.

Other teams use their own password managers, those are not documented
here, see [issue 29677](https://gitlab.torproject.org/tpo/tpa/team/-/issues/29677) for a discussion on that.

[[_TOC_]]

# Tutorial

## Basic usage

Once you have a local copy of the repository and have properly
configured your environment (see [installation](#installation)), you should be
able to list passwords, for example:

    pass ls

or, if you are in a subdirectory:

    pass ls tor

To copy a password to the clipboard, use:

    pass -c tor/services/rt.torproject.org

Passwords are sorted in different folders, see the [folder
organisation](#folder-organisation) section for details.

## One-time passwords

To access certain sites, you'll need a one-time password which is
stored in the password manager. This can be done with the
[pass-otp](https://github.com/tadfisher/pass-otp) extension. Once that is installed, you should use the
"clipboard" feature to copy-paste the one time code, with:

    pass otp -c tor/services/example.com

## Adding a new secret

To add a new secret, use the `generate` command:

    pass generate -c services/SECRETNAME

That will generate a strong password and store it in the `services/`
folder, under the name `SECRETNAME`. It will also copy it to the
clipboard so you can paste it in a password field elsewhere, for
example when creating a new account.

If you cannot change the secret and simply need to store it, use the
`insert` command instead:

    pass insert services

That will ask you to confirm the password, and supports only entering
a single line. To enter multiple lines, use the `-m` switch.

Passwords are sorted in different folders, see the [folder
organisation](#folder-organisation) section for details.

Make sure you *push* after making your changes! By default, `pass`
doesn't synchronize your changes upstream:

    pass git push

## Rotating a secret

To regenerate a password, you can reuse the same mechanism as the
[adding a new secret](#adding-a-new-secret) procedure, but be warned that this will
completely overwrite the entry, including possible comments or extra
fields that might be present.

# How-to

## On-boarding new staff

When a new person comes in, their key needs to be added to the
`.gpg-id` file. The easiest way to do this is with the `init`
command. This, for example, will add a new fingerprint to the file:

    cd ~/.password-store
    pass init $(cat .gpg-id 0000000000000000000000000000000000000000)

The new fingerprint must also be allowed to sign the key store:

    echo "export PASSWORD_STORE_SIGNING_KEY=\"$(cat ~/.password-store/.gpg-id)\"" >> ~/.bashrc

## Off boarding

When staff that has access to the password store leaves, access to the
password manager needs to be removed.

But that is not sufficient to protect the passwords, as the person
will still have a local copy of the passwords (and could have copied
them elsewhere anyway). So all passwords the person had access to must
be rotated.

See [mass password rotation](#mass-password-rotation) procedures.

## Mass password rotation

It's possible (but very time consuming) to rotate multiple passwords
in the store. For this, the [pass-update](https://github.com/roddhjav/pass-update/) tool is useful, as it
automates part of the process. It will:

 1. for all (or a subset of) passwords
 2. copy the current password to the clipboard (or show it)
 3. wait for the operator to copy-paste it to the site
 4. generate and save a new password, and copy it to the clipboard

So a bulk update procedure looks like this:

    pass update -c

That will take a long time to proceed those, so it's probably better
to do it one service at a time. Here's documentation specific to each
section of the password manager. You should prioritize the `dns` and
`hosting` sections.

See [issue 41530](https://gitlab.torproject.org/tpo/tpa/team/-/issues/41530) for a mass-password rotation run.

### DNS and hosting

Those two are similar and give access to critical parts of the
infrastructure, so they are worth processing first. Start with current
hosting and DNS providers:

    pass update -c dns/joker dns/portal.netnod.se hosting/accounts.hetzner.com hosting/app.fastly.com

Then the rest of them:

    pass update -c hosting

### Services

Those are generally websites with special accesses. They are of a
lesser priority, but should nevertheless be processed:

    pass update -c services

It might be worth examining the service list to prioritize some of
them.

### root

Next, the root passwords need to be rotated.

TODO: fabric task?

### OOB

Similarly, out-of band access need to be reset. This involves logging
in to each server's BIOS and changing the password. `pass update`,
again, should help, but instead of going through a web browser, it's
likely more efficient to do this over SSH:

    pass update -c oob

### LUKS

Next, full disk encryption keys.

TODO: fabric task?

### lists

Individual list passwords may be rotated, but that's a lot of trouble
and coordination. The site password should be changed, at least. When
Mailman 3 is deployed, all those will go away anyway.

### misc

Those can probably be left alone; it's unclear if they have any
relevance left and should probably be removed.

## Pager playbook

This service is likely not going to alert or require emergency
interventions.

## Disaster recovery

A total server loss should be relatively easy to recover from. Because
the password manager is backed by git, it's "simply" a matter of
finding another secure location for the repository, where only the TPA
admins have access to the server.

TODO: document a step-by-step procedure to recreate a minimal git
server or exchange updates to the store. Or Syncthing or Nextcloud
maybe?

If the `pass` command somehow fails to find passwords, you *should* be
able to decrypt the passwords with GnuPG directly. Assuming you are in
the password store (e.g. `~/.password-store/tor`), this should work:

    gpg -d < luks/servername

If that fails, it should tell you which key the file is encrypted
to. You need to find a copy of that private key, somehow.

# Reference

<!-- this section is a more in-depth review of how this service works, -->
<!-- how it's setup. day-to-day operation should be covered in -->
<!-- tutorial or how-to, this is more in-depth -->

<!-- a good guide to "audit" an existing project's design: -->
<!-- https://bluesock.org/~willkg/blog/dev/auditing_projects.html -->
<!-- the following sections are partially based on that -->

## Installation

The upstream [download instructions](https://www.passwordstore.org/#download) should get you started with
installing `pass` itself. But then you need a local copy of the
repository, and configure your environment.

First, you need to get access to the password manager which is
currently hosted on the legacy Git repository:

    git clone git@git-rw.torproject.org:admin/tor-passwords.git ~/.password-store

If you do not have access, it's because your onboarding didn't happen
correctly, or that this guide is not for you.

Note that the above clones the password manager directly under the
default password-store path, in `~/.password-store`. If you are
*already* using `pass`, there's likely already things there, so you
will probably want to clone it in a subdirectory, like this:

    git clone git@git-rw.torproject.org:admin/tor-passwords.git ~/.password-store/tor

You can also clone the password store elsewhere and use a symbolic
link to `~/.password-store` to reference it.

If you have such a setup, you will probably want to add a `pre-push`
(sorry, there's no `post-push`, which would be more appropriate) hook
so that `pass git push` will also push to the sub-repository:

    cd ~/.password-store &&
    printf '#!/bin/sh\nprintf "echo pushing tor repository first... "\ngit -C tor push || true\n' > .git/hooks/pre-push &&
    chmod +x .git/hooks/pre-push

Make sure you configure pass to verify signatures. This can be done by
adding a `PASSWORD_STORE_SIGNING_KEY` to your environment, for
example, in bash:

    echo "export PASSWORD_STORE_SIGNING_KEY=\"$(cat ~/.password-store/.gpg-id)\"" >> ~/.bashrc

Note that this takes the signing key from the `.gpg-id` file. You
*should* verify those key fingerprints and definitely *not*
automatically pull them from the `.gpg-id` file regularly. The above
command will actually write the fingerprints (as opposed to using `cat
.gpg-id`) to the configuration file, which is safer as an attacker
would need to modify your configuration to take over the repository.

## Migration from pwstore

The password store was initialized with this:

    export PASSWORD_STORE_DIR=$PWD/tor-passwords
    export PASSWORD_STORE_SIGNING_KEY="BBB6CD4C98D74E1358A752A602293A6FA4E53473 95F341D746CF1FC8B05A0ED5D3F900749268E55E E3ED482E44A53F5BBE585032D50F9EBC09E69937"
    pass init $PASSWORD_STORE_SIGNING_KEY

This created the `.gpg-id` metadata file that indicates which keys to
use to encrypt the files. It also signed the file (in `.gpg-id.sig`).

Then the basic categories were created:

    mkdir dns hosting lists luks misc root services

misc files were moved in place:

    git mv entroy-key.pgp misc/entropy-key.gpg
    git mv ssl-contingency-keys.pgp misc/ssl-contingency-keep.gpg
    git mv win7-keys.pgp misc/win7-keys.gpg

Note that those files were renamed to `.gpg` because pass relies on
that unfortunate naming convention (`.pgp` is the standard file
extension for encrypted files).

The root passwords were converted with:

```
gpg -d < hosts.pgp | sed '0,/^host/d'| while read host pass date; do 
    pass insert -m root/$host <<EOF
    $pass
    date: $date
    EOF
done
```

Integrity was verified with:

    anarcat@angela:tor-passwords$ gpg -d < hosts.pgp | sed '0,/^host/d'| wc -l 
    gpg: encrypted with 2048-bit RSA key, ID 41D1C6D1D746A14F, created 2020-08-31
          "Peter Palfrader"
    gpg: encrypted with 255-bit ECDH key, ID 16ABD08E8129F596, created 2022-08-16
          "Jérôme Charaoui <jerome@riseup.net>"
    gpg: encrypted with 255-bit ECDH key, ID 9456BA69685EAFFB, created 2023-05-30
          "Antoine Beaupré <anarcat@torproject.org>"
    88
    anarcat@angela:tor-passwords$ ls root/| wc -l
    88
    anarcat@angela:tor-passwords$ for p in $(ls root/* | sed 's/.gpg//') ; do if ! pass $p | grep -q date:; then echo $p has no date; fi ; if ! pass $p | wc -l | grep -q '^2$'; then echo $p does not have 2 lines; fi ; done
    anarcat@angela:tor-passwords$

The `lists` passwords were converted by first going through the YAML
to fix lots of syntax errors, then doing the conversion with a Python
script written for the purpose, in `lists/parse-lists.py`.

The passwords in all the other stores were converted using a mix of
manual creation and rewriting the files to turn them into a shell
script. For example, an entry like:

```yaml
foo:
  access: example.com
  username: root
  password: REDACTED
bar:
  access: bar.example.com
  username: root
  password: REDACTED
```

would be rewritten, either by hand or with a macro (to deal with
multiple entries more easily), into:

```shell
pass inert -m services/foo <<EOF
REDACTED
url: example.com
user: root
EOF
pass inert -m services/bar <<EOF
REDACTED
url: bar.example.com
user: root
EOF
```

In the process, fields were reordered and renamed. The following
changes were performed manually:

 * `url` instead of `access`
 * `user` instead of `username`
 * `password:` was stripped and the password was put alone on a the
   first line, as pass would expect
 * TOTP passwords were turned into [`oauth://` URLs](https://github.com/google/google-authenticator/wiki/Key-Uri-Format), but the
   previous incantation was kept as a backup, as that wasn't tested
   with `pass-otp`

The OOB passwords were split from the LUKS passwords, so that we can
have only the LUKS password on its own in a file. This will also
possibly allow layered accesses there where some operators could have
access to the BIOS but not the LUKS encryption key. It will also make
it easier to move the encryption key elsewhere if needed.

History was retained, for now, as it seemed safer that way. The
`pwstore` tag was laid on the last commit before the migration, if we
ever need an easy way to roll back.

## Upgrades

Pass is managed client side, and packaged widely. Upgrades have so far
not included any breaking changes and should be safe to automate using
normal upgrade mechanisms.

## SLA

No specific SLA for this service.

## Design and architecture

<!-- how this is built -->
<!-- should reuse and expand on the "proposed solution" discussed in -->
<!-- a previous RFC or the Discussion section below. it's a -->
<!-- "as-built" documented, whereas the "Proposed solution" is an -->
<!-- "architectural" document, which the final result might differ -->
<!-- from, sometimes significantly -->

## Services

<!-- open ports, daemons, cron jobs -->

## Storage

<!-- databases? plain text file? the frigging blockchain? memory? -->

## Queues

<!-- email queues, job queues, schedulers -->

## Interfaces

<!-- e.g. web APIs, commandline clients, etc -->

## Authentication

<!-- SSH? LDAP? standalone? -->

## Implementation

<!-- programming languages, frameworks, versions, license -->

## Related services

<!-- dependent services (e.g. authenticates against LDAP, or requires -->
<!-- git pushes)  -->

## Issues

<!-- such projects are never over. add a pointer to well-known issues -->
<!-- and show how to report problems. usually a link to the -->
<!-- issue tracker. consider creating a new Label to regroup the -->
<!-- issues if using the general tracker. see also TPA-RFC-19. -->

There is no issue tracker specifically for this project, [File][] or
[search][] for issues in the [team issue tracker][search] with the
label ~Foo.

 [File]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/new
 [search]: https://gitlab.torproject.org/tpo/tpa/team/-/issues?label_name%5B%5D=Foo

## Maintainer

<!-- document who deployed and operates this service, the team and -->
<!-- ideally the person inside that team -->

## Users

<!-- who the main users are, how they use the service. possibly reuse -->
<!-- the Personas section in the RFC, if available. -->

## Upstream

<!-- who the upstreams are, if they are still active, -->
<!-- collaborative, how do we keep up to date, support channels, see -->
<!-- also the "Issues" section above -->

## Monitoring and metrics

<!-- describe how this service is monitored, how security issues and -->
<!-- upgrades are tracked, see also "Upgrades" above. -->

## Tests

<!-- how the service can be tested, for example after major changes -->
<!-- like IP address changes or upgrades. describe CI, test suites, linting -->

## Logs

<!-- where are the logs? how long are they kept? any PII? -->
<!-- what about performance metrics? same questions -->

## Backups

<!-- does this service need anything special in terms of backups? -->
<!-- e.g. locking a database? special recovery procedures? -->

## Other documentation

<!-- references to upstream documentation, if relevant -->

# Discussion

<!-- the "discussion" section is where you put any longer conversation -->
<!-- about the project that you will not need in a casual -->
<!-- review. history of the project, why it was done the way it was -->
<!-- (as opposed to how), alternatives, and other proposals are -->
<!-- relevant here. -->

<!-- this at least partly overlaps with the TPA-RFC process (see -->
<!-- policy.md), but in general should defer to proposals when -->
<!-- available -->

## Overview

<!-- describe the overall project. should include a link to a ticket -->
<!-- that has a launch checklist -->

<!-- if this is an old project being documented, summarize the known -->
<!-- issues with the project. -->

## Security and risk assessment

<!--

 5. When was the last security review done on the project? What was
    the outcome? Are there any security issues currently? Should it
    have another security review?

 6. When was the last risk assessment done? Something that would cover
    risks from the data stored, the access required, etc.

-->

## Technical debt and next steps

<!--

 7. Are there any in-progress projects? Technical debt cleanup?
    Migrations? What state are they in? What's the urgency? What's the
    next steps?

 8. What urgent things need to be done on this project?

-->

## Proposed Solution

<!-- Link to RFC -->

## Other alternatives

<!-- include benchmarks and procedure if relevant -->
