[[_TOC_]]

Tor Project is using [NextCloud](https://nc.torproject.net/) as a tool for managing and sharing
resources and for collaborative editing.

Questions and bug reports are handled by Tor's NextCloud service admin
team. For bug reports, please [create a ticket][] in the
`Service - nextcloud` component in [Trac](https://trac.torproject.org/). For questions, find us
on IRC (GeKo, ln5, pospeselr, anarcat, gaba) or send email to
`nextcloud-admin@torproject.org`.

[create a ticket]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/new

# Tutorial

## Signing in and setting up two-factor authentication

 1. Find an email sent to your personal Tor Project email address from
    `nc@riseup.net` with a link to `https://nc.torproject.net/`
 1. Do not click on the link in the email, clicking on links in emails
    is dangerous! Instead, use the safe way: copy and paste the link
    in the email into your web browser.
 1. Follow the instructions for changing your passphrase.
 1. Enable [two-factor authentication](https://en.wikipedia.org/wiki/Two-factor_authentication) (2FA):

    1. Pick either a [TOTP](https://en.wikipedia.org/wiki/Time-based_One-time_Password_algorithm) or [U2F device](https://en.wikipedia.org/wiki/Universal_2nd_Factor) as an "second
       factor". TOTP is often done with an app like [Google
       Authenticator](https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2) or a free alternative (for example [free OTP
       plus](https://github.com/helloworld1/FreeOTPPlus/), see also this [list from the Nextcloud
       project](https://github.com/nextcloud/twofactor_totp#readme)). U2F is usually supported by security tokens like
       the [YubiKey](https://en.wikipedia.org/wiki/YubiKey), [Nitrokey](https://www.nitrokey.com/), or similar.
    1. If you have a TOTP setup, locate it and then:
       1. Click "Enable TOTP" on the web page.
       1. Insert your token or start the TOTP application on your
          handheld device and scan the QR code displayed on the web
          page.
       1. Enter the numbers from the token/application into the text
          field on the web page.
       1. Log out and log in again, to verify that you got two factor
          authentication working.
    1. If you have a U2F setup, locate it and then:
       1. Click the "Add U2F device" button under the "U2F device"
          section
       1. Insert the token and press the button when prompted by your
          web browser
       1. Enter a name for the device and click "Add"
       1. Log out and log in again, to verify that you got two factor
          authentication working.
    1. In NextCloud, select Settings -> Security. The link to your
       settings can be found by clicking on your "user icon" in the top
       right corner. Direct link: [Settings -> Security](https://nc.torproject.net/settings/user/security).
    1. Click "Generate Backup codes" in the Two-Factor Authentication
       section of that page.
    1. Save your backup codes to a password manager of your
       choice. These will be needed to regain access to your NextCloud
       account if you ever lose your 2FA token/application.

## A note on credentials

**_Don't let other people use your credentials.**_ Not even people you
know and like. If you know someone who should have a NextCloud
account, let the service admins know in [a ticket][create a ticket].

**_Don't let other _other_ people use your credentials.**_ Never enter
your passphrase or two-factor code on any other site than Tor
Project's NextCloud site. Lower the risk of entering your credentials
to the wrong site by verifying that there's a green padlock next to
the URL and that the URL is indeed correct.

**_Don't lose your credentials.**_ This is especially important since
files are _encrypted_ in a key derived from your passphrase. To help
deal with when a phone or hardware token is lost, you should really
(really!) generate **Backup codes** and store those in a safe place,
together with your passphrase. Backup codes can be used to restore
access to your NextCloud and encrypted files. There is no other way of
accessing encrypted files! Backup codes can be generated from the
[Settings -> Security](https://nc.torproject.net/settings/user/security) page.

## Files

In the top left of the header-bar, you should see a "Folder" icon;
when moused over a text label should appear beneath it that says
Files. When clicked, you will be taken to the **Files** app and placed
in the root of your NextCloud file directory. Here, you can upload
local files to NextCloud, download remote files to your local storage,
and share remote files across the internet. You can also perform the
various file management operations (move, rename, copy, etc) you are
familiar with in Explorer on Windows or Finder on macOS.

On the left side of the **Files** app there is a side-bar with a few
helpful views of your files.

 * All files : takes you to your root folder
 * Recent : recently accessed files and folders
 * Favorites : bookmarked files and folders
 * Shares : files and folders that have been shared with you or you
   are sharing with others
 * Tags : search for files and folders by tag

### Upload a file

Local files saved on your computer can be uploaded to NextCloud. To
upload a file:

 1. In the NextCloud **Files** app, navigate to the folder where you
    want to store the file
 1. Click on the circular button with a **+** inside it (to the right
    of the little house icon)
 1. Click **Upload file** entry in the context menu
 1. Select a file to upload using your system's file browser window

### Share a file or directory with another NextCloud user

Files stored in your NextCloud file directory can be selectively
shared with other NextCloud users. To share a file:

 1. Locate the file you wish to share (either by navigating to the
    folder it is in, by searching, or by using one of the views in the
    sidebar).
 1. Click the file's **Share** icon (to the right of the file name)
 1. In the pane that pops out from the right, click on the search box
    labeled **Name, federated cloud ID or email address…**
 1. Search for the user or group you wish to share with by NextCloud
    user id (pospeselr), email address (richard@torproject.org), or
    name (Richard Pospesel) and select them from the dropdown.
 1. Optional: click on the meatball menu to the right of the shared
    user and edit the sharing options associated with the file or
    directory.
   * For instance, you may wish to automatically un-share the file at
     some point in the future

### Share a file with the internet

Files can also be shared with the internet via a url. Files shared in
this fashion are read-only by default, but be mindful of what you
share: **by default, anyone who knows the link url can download the
file**. To share a file:

 1. Locate the file you wish to share
 1. Click the file's **Share** icon (to the right of the file name)
 1. In the pane that pops out from the right, click the **+** icon
    beside the **Share link** entry
 1. Select appropriate sharing options in the context menu (these can
    be changed later without invalidating the link)
 1. Optional: A few measures to limit access to a shared file:
   * Prevent general access by selecting the **Password protect**
     option
   * Automatically deactivate the share link at a certain time by
     selecting the **Set expiration date** option
 1. Finally, copy the shared link to your clipboard by clicking on the
    **Clipboard** icon

### Un-share files or edit their permissions

If you have shared files or folders with either the internet or
another NextCloud user, you can un-share them. To un-share a file:

 1. Locate the file you wish to un-share in the **Files** app
   * All of your currently shared files and folders can be found from the **Shares** view
 1. Click the file's **Shared** icon (to the right of the file name)
 1. In the pane that pops out from the right, you get a listing of all
    of the users and share links associated with this file
 1. Click the meatball menu to the right of one of these listings to
    edit share permissions, or to delete the share entirely

### File management

### Search for a file

In the Files application press Ctrl+F, or click the magnifying glass
at the upper right of the screen, and type any part of a file name.

### Desktop support

Files can be addressed transparently through [WebDAV](https://en.wikipedia.org/wiki/WebDAV). Most file
explorer support the protocol which should enable you to browse the
files natively on your desktop computer. Detailed instructions on how
to setup various platforms are available in the [main Nextcloud
documentation site about WebDAV](https://docs.nextcloud.com/server/16/user_manual/files/access_webdav.html).

But the short version is you can find the URL in the "Settings wheel"
at the bottom right of the files tab, which should look something like
`https://nc.torproject.net/remote.php/webdav/`. You might have to
change the `https://` part to `davs://` or `webdavs://` depending on
the desktop environment you are running.

If you have setup 2FA (two-factor authentication), you will also need
to setup an "app password". To set that up:

 1. head to your personal settings by clicking on your icon on the top
    right and then `Settings`
 1. click the `Security` tab on the right
 1. in the `Devices & sessions` section, fill in an "app name" (for
    example, "Nautilus file manager on my desktop") and click `Create
    new app password`
 1. copy-paste the password and store it in your password manager
 1. click `done`

The password can now be used in your WebDAV configuration. If you fail
to perform the above configuration, WebDAV connections will fail with
an `Unauthorized` error message as long as 2FA is configured.

## Collaborative editing of a document

Press the plus button at the top of the file browser, it brings you a
pull-down menu where you can pick "Document", "Spreadsheet",
"Presentation". When you click one of those, it will become an
editable field where you should put the name of the file you wish to
create and hit enter, or the arrow.

### A few gotchas with collaborative editing

Behind the scenes, when a user opens a document for editing, the
document is being copied from the NextCloud server to the document
editing server. Once all editing sessions are closed, the document is
being copied back to NextCloud. This behavior makes the following
information important.

 * **_The document editing server copies documents from NextCloud**_,
   so while a document is open for editing it will differ from the
   version stored in NextCloud. The effect of this is that downloads
   from NextCloud will show a different version than the one currently
   being edited.

 * **_A document is stored back to NextCloud 10 seconds after all
   editing sessions for that document have finished.**_ This means
   that as long as there's a session open, active or idle, the
   versions will differ.  If either the document server breaks or the
   connection between NextCloud and the document server breaks it is
   possible that there'll be data loss.

 * **_An idle editing session expires after 5 minutes.**_ This helps
   making sure the document will not hang indefinitely in the document
   editing server even if a user leaves a browser tab open.

## Client software for both desktop (Window, macOS,Linux) and handheld (Android and iPhone)

https://nextcloud.com/clients/

## Using calendars for appointments and tasks

TODO

### Importing a calendar feed from Google

 1. In your Google calendar go to the "Settings and Sharing" menu
    (menu appears by hovering over the right hand side of your
    calendar's name - "Options for " and the calendar name) for the
    calendar feed you want to import.
 1. Scroll down to the "Integrate Calendar" section and copy the
    "Secret address in iCal format" value.
 1. In Nextcloud, click on "New Subscription" and paste in the
    calendar link you copied above.

## Calendar clients

Nextcloud has extensive support for events and appointments in its
Calendar app. It can be used through the web interface, but since it
supports the CalDAV standard, it can also be used with other
clients. This section tries to guide our users towards some solutions
which could be of interest.

### Android

First create a Nextcloud "App" password by logging into the Nextcloud web interface, and then go to your profile->Settings->Security->Create a new App Password. Give it a name and then copy the randomly generated password (you cannot see the password again after you are finished!), then click Done.

Install DAVx^5 from [F-Droid](https://f-droid.org/en/packages/at.bitfire.davdroid/) or the [Playstore](https://play.google.com/store/apps/details?id=at.bitfire.davdroid&referrer=utm_source%3Dhomepage) This program will synchronize with nextcloud your calendars and contacts and is Free. Launch it and press the "+" to add a new account. Pick "Login with URL and username". Set Base URL: "nc.torproject.org", put your nextcloud username into "Username" and then the App password that you generated previously into the "Password" field, click Login. Under Create Account, make your Account name your email address, then click Create Account. Then click the CalDAV tab and select the calendars you wish to sync and then press the round orange button with the two arrows in the bottom right to begin the synchronization. You can also sync your contacts, if you store them in Nextcloud, by clicking the CardDav tab and selecting things there.

For more information, [check the Nextcloud documentation](https://docs.nextcloud.com/server/latest/user_manual/en/groupware/sync_android.html)

### iOS

This is a specific configuration for those that have
two-factor-authentication enabled on their account.

 1. Go to your Nextcloud account
 2. Select Settings
 3. On the left bar, select Security
 4. A list of topics will appear: “Password, Two-factor
    Authentication, Passwordless Authentication, Devices & Session”
 5. Go to Devices & Session, on the field “App name” create a name for
    your phone, like “iPhone Calendar” and click on “Create new app
    password”
 6. A specific password will be created to sync your Calendar on your
    phone, note that this password will only be shown this one time.

Then, you can follow the Nextcloud settings, take your phone:

 1. Go to your phone Settings
 2. Select Calendar
 3. Select Accounts
 4. Select Add Account
 5. Select Other as account type
 6. Select Add CalDAV account
 7. For server, type the domain name of your server i.e. example.com.
 8. Enter your user name and the password that was just created to
    sync your account.
 9. Select Next.

Done!

Note: the above instructions come from [this tutorial by Narrira
Lemos](https://narrira.medium.com/sync-your-nextcloud-calendar-on-iphone-398aecc2d5bc).

### Mac, Windows, Linux: Thunderbird

[Thunderbird](https://www.thunderbird.net), made by the [Mozilla foundation](https://mozilla.org), has a [built-in calendar](https://www.thunderbird.net/en-US/calendar/). This used to be a separate extension called Lightning, but it is now integrated into Thunderbird itself.

It's a good choice if you already use Thunderbird, but you can also use it as a calendar if you do not use Thunderbird.

In order to use the calendar, you need to first generate an App password, and then install a couple of add-ons. 

First create a Nextcloud "App" password by logging into the Nextcloud web interface, and then go to your profile->Settings->Security->Create a new App Password. Give it a name and then copy the randomly generated password (you cannot see the password again after you are finished!), then click Done. Note: if you did this previously for Android, its not a bad idea to have a separate App Password for Thunderbird, that way you can revoke the Android password, if you lose your device, and still have access to your Thunderbird calendar.

Simply go to Tools->Addons and Themes->Search->Tbsync install. Then in the Tbsync preferences -> Account Actions -> Add new account -> Caldav & CardDav -> install the plugin for this connector.

Then under the Tbsync preferences, you pick "Account actions" in the lower left, then "Add new account" -> "CalDAV & CardDAV". Then pick "Automatic Configuration". Put an account name (your choice), your Nextcloud username, your App Password you generated previously and then for Server URL, just put "nc.torproject.org" and click "Next".

Now just select the calendars you wish to synchronize and click "Synchronize now", and you should be all set.

If you have trouble finding the calendar in Thunderbird, it can be a little bit hidden. Just look for the little icon in the upper right that says "Switch to the calendar tab" when you hover over it.

### Linux: GNOME Calendar, KDE Korganizer

[GNOME]() has a [Calendar](https://wiki.gnome.org/Apps/Calendar) and KDE has [Korganizer](https://apps.kde.org/korganizer/), which may
be good choices depending on your favorite Linux desktop.

Untested. GNOME Calendar doesn't display time zones which is probably
a deal breaker.

### Commandline tools: vdirsyncer, ikhal, calcurses

[vdirsyncer](https://github.com/pimutils/vdirsyncer) is the hardcode, commandline tool to synchronize
calendars from a remote CalDAV server to a local directory, and
back. It does nothing else. vdirsyncer is somewhat tricky to configure
and to use, and doesn't deal well with calendars that disappear.

To read calendars, you would typically use something like [khal](https://lostpackets.de/khal/),
which works well. Anarcat sometimes uses ikhal and vdirsyncer to read
his calendars.

Another option is [calcurses](https://calcurse.org/) which is similar to ikhal but has
"experimental CalDAV support". Untested.

## Managing contacts

TODO

# How-to

## Showing UTC times in weekly calendar view

This [TimeZoneChallenged.user.js](https://people.torproject.org/~ma1/userscripts/TimeZoneChallenged.user.js) [GreaseMonkey script](https://www.greasespot.net/) allows
you to see the UTC time next to your local time in the left column of
the Nextcloud Calendar's "weekly" view.

To install it:

 1. [install the Greasmonkey add-on](https://addons.mozilla.org/firefox/addon/greasemonkey/) if not already done
 2. in the extension, select "new user script"
 3. copy paste the above script and save
 4. in the extension, select the script, then "user script options"
 5. in "user includes", add `https://nc.torproject.net/*`

Ideally, this would be builtin to Nextcloud, see [this discussion](https://help.nextcloud.com/t/show-multiple-time-zones/137287)
and [this issue](https://github.com/nextcloud/calendar/issues/4254) for followup.

## Resetting 2FA for another user

If someone manages to lock themselves out of their two-factor
authentication, they might ask you for help. 

First, you need to make absolutely sure they are who they say they
are. Typically, this happens with an OpenPGP signature of a message
that states the current date and the actual desire to reset the 2FA
mechanisms. For example, a message like this:

    -----BEGIN PGP SIGNED MESSAGE-----

    i authorize a Nextcloud admin to reset or disable my 2FA credentials on
    nc.torproject.net for at most one week. now is 2022-01-31 9:33UTC

    -----BEGIN PGP SIGNATURE-----
    [...]
    -----END PGP SIGNATURE-----

This is to ensure that such a message cannot be "replayed" by an
hostile party to reset 2FA for another user. 

Once you have verified the person's identity correctly, you need to
"impersonate" the user and reset their 2FA, with the following path:

 1. log into Nextcloud
 2. hit your avatar on the top-right
 3. hit "Users"
 4. find the user in the list (hint: you can enter the username or
    email on the first row)
 5. hit the little "three dots" (`...`) button on the right
 6. pick "impersonate", you are now logged in as that person (be
    careful!)
 7. hit the avatar on the top-right again
 8. select "Settings"
 9. on the left menu, select "Security"
 10. click the "regenerate backup codes" button and send them one of
     the codes, encrypted

When you send the recovery code, make sure to advise the user to
regenerate the recovery codes and keep a copy somewhere. This is a
good template to use:

```
Hi!

Please use this 2fa recovery code to login to your nextcloud account:

[INSERT CODE HERE]

Once you are done, regenerate the recovery codes (Avatar -> Settings ->
Security) and save a copy somewhere safe so this doesn't happen again!
```

## FAQ

### Why do we not use server-side encryption?

Example question:

> I saw that we have server-side encryption **disabled** in our
> configuration. That seems bad. Isn't encryption good? Don't we want
> to be good?

Answer:

Server-side encryption doesn't help us with our current setup. We're
hosting the Nextcloud server and its files at the same provider.

If we would be (say) hosting the server at provider A and the files at
(say) provider B, that would give us some protection because an
provider B compromise wouldn't compromise the files. But that's not
our configuration, so server-side encryption doesn't give us
additional security benefits.
