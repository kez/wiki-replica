---
title: TPA-RFC-X: RFC template
---

[[_TOC_]]

Summary: this template describes the basic field a TPA-RFC policy
should have. Refer to [policy/tpa-rfc-1-policy](policy/tpa-rfc-1-policy) for the actual policy requirements.

# Background

# Proposal

## Goals

<!-- include bugs to be fixed -->

### Must have

### Nice to have

### Non-Goals

## Scope

## Affected users

# Examples or Personas

Examples:

 * ...

Counter examples:

 * ...

# Alternatives considered

# Costs

# Approval

# Deadline

# Status

This proposal is currently in the `draft` state.

# References

See [policy/tpa-rfc-1-policy](policy/tpa-rfc-1-policy).
