---
title: TPA-RFC-61: 2024 roadmap
---

[[_TOC_]]

Summary: a roadmap for 2024

# Proposal

## Priorities for 2024

### Must have

* bookworm upgrade completion (50% done) before July 2024 (so Q1-Q2
  2024), which includes:
  * puppet server 7 upgrade: Q2 2024?
  * mailman 3 and schleuder upgrade (probably on a new mail server),
    hopefully Q2 2024
  * inciga retirement / migration to Prometheus Q3-Q4 2024?
* old services retirement
  * SVN retirement (or not): proposal in Q2, execution Q3-Q4?
    Nextcloud will not work after all because of [major issues with
    collaborative editing](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/nextcloud#a-few-gotchas-with-collaborative-editing), need to go back to the drawing board.
  * gitolite retirement, which includes:
    * 12 TPA repos to migrate, some complicated
    * archiving all other repositories
    * lockdown scheduled for Q2 2024
* email services? TPA-RFC-45 includes:
  * draft TPA-RFC-45, which may include:
  * mailbox hosting in HA
* minio clustering and backups
* make a decision on gitlab ultimate

### nice to have

* Puppet CI
* review TPA-RFC process
* tiered gitlab runners
* improve upgrade ([tpo/tpa/team#41485](https://gitlab.torproject.org/tpo/tpa/team/-/issues/41485)) and install
  ([tpo/tpa/team#31239](https://gitlab.torproject.org/tpo/tpa/team/-/issues/31239)) automation
* disaster recovery planning ([tpo/tpa/team#40628](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40628))
* monitor technical debt ([tpo/tpa/team#41456](https://gitlab.torproject.org/tpo/tpa/team/-/issues/41456))
* review team function and scope (TPA? web? SRE?)

### black swans

A [black swan event](https://en.wikipedia.org/wiki/Black_swan_theory) is "an event that comes as a surprise, has a
major effect, and is often inappropriately rationalized after the fact
with the benefit of hindsight" ([Wikipedia](https://en.wikipedia.org/wiki/Black_swan_theory)). In our case, it's
typically an unexpected and unplanned emergency that derails the above
plans.

Here are possible changes that are technically *not* black swans
(because they are listed here!) but that could serve as placeholders
for the actual events we'll have this year:

* Hetzner evacuation (plan and estimates)
* outages, capacity scaling
* in general, disaster recovery plans
* possible future changes for internal chat (IRC onboarding?) or sudden
  requirement to self-host another service currently hosted externally
* some guy named jerry, who knows!

## THE WEB - how we organize it this year

This still need to be discussed and reviewed with isa.

- call for a "web team meeting"
- discuss priorities with that team
- discuss how we are going to organize ourselves
- announce the hiring this year of a web dev

# Approval

Approval by TPA, submitted to @isabela.

# Deadline

February 1st.

# Status

This proposal is currently in the `proposed` state.

# References

Previous roadmap [established in TPA-RFC-42](policy/tpa-rfc-42-roadmap-2023) and is in [roadmap/2023](roadmap/2023).
