---
title: TPA-RFC-51: Improve l10n review ci workflow
---

[[_TOC_]]

Summary: adopt a new l10n review workflow that removes the need for the
weblate bot/user to have the `Maintainer` role on all of our translated website
repositories.

# Background

We recently [switched from Transifex to Weblate][] as the official translation
platform for our multi-language Lektor websites. As part of the transition, a
new bot account was created on GitLab, [weblate][]. The purpose of this account
is to allow the [Weblate platform][] to push commits containing new or updated
strings to our GitLab's [translation repository][].

When this occurs, GitLab CI builds a special "l10n-review" version of the
website that has all minimally-translated languages enabled. This allows two
things: the ability for translators to view their work in context and for
localization coordinator to evaluate the quality of unpublished translations.

Unfortunately, because the builds occur on the `main` branch, the `weblate` user
account must be granted the `Maintainer` role, which isn't ideal because this
grants a third party (Weblate) significant permissions over several important
GitLab projects.

![current l10n review ci workflow](tpa-rfc-51-improve-l10n-review-ci-workflow/current-workflow.png)

[switched from Transifex to Weblate]: https://gitlab.torproject.org/tpo/community/l10n/-/issues/40089
[weblate]: https://gitlab.torproject.org/weblate
[Weblate platform]: https://weblate.org/
[translation repository]: https://gitlab.torproject.org/tpo/translation

# Proposal

The proposal here is to effect the following changes:

 - Create new projects/repositories for all l10n-enabled websites under the
   `tpo/web/l10n` namespace (all features disabled except Repository and CI)
 - Configure push mirroring between the "main" and "l10n" repos using SSH keys
 - Modify the build, test and deploy [Lektor CI job templates][] to ensure they
   don't execute on the mirror's CI
 - Change each website's `*-contentspot` branch to make `.gitlab-ci.yml` trigger
   pipelines in the mirror project instead of the main one
 - Grant the `Maintainer` role to the `weblate` user account on the mirror and
   remove it from the main project

As a proof of concept, this has been done for the [gettor-web][] project. The
mirror project for l10n reviews is located at [tpo/web/l10n/gettor-web][].

[Lektor CI job templates]: https://gitlab.torproject.org/tpo/tpa/ci-templates/-/blob/main/lektor.yml
[gettor-web]: https://gitlab.torproject.org/tpo/web/gettor-web
[tpo/web/l10n/gettor-web]: https://gitlab.torproject.org/tpo/web/l10n/gettor-web

![proposed l10n review ci workflow](tpa-rfc-51-improve-l10n-review-ci-workflow/proposed-workflow.png)

## Goals

The goal is for the `weblate` user to be able to run its CI pipelines
successfully, deploying l10n-review builds to `review.torproject.net`, without
the need for the account to have `Maintainer` role in the main project.

As a nice-to-have goal, CI pipelines for l10n review builds and deployments
would be separate from the development and MR-preview pipelines. This means the
list of CI pipelines in each project would no longer be cluttered with l10n
frequent related pipelines (as seen currently) but would only contain MR and
`main`-branch CI pipelines.

## Scope

The scope for this RFC is all l10n-enabled Lektor websites under `tpo/web`.

# Alternatives considered

The main alternative here would be to accept the security risk: the Weblate bot
might go haywire and wreck havoc on our websites. While dealing with this would
be highly annoying, there's no reason to think we couldn't recover relatively
quickly from backups.

Another alternative here would be to wait for GitLab to eventually roll out the
ability for non-`Maintainer` accounts to execute pipelines on protected
branches. The problem is, according to GitLab's [own issue tracker][], this
isn't happening anytime soon.

[own issue tracker]: https://gitlab.com/gitlab-org/gitlab/-/issues/230939

# Costs

Only the labor associated with implementing the changes: estimated between one
and two hours.

# Approval

This needs the approval of TPA, GitLab admins and the translation coordinator.

# Deadline

This RFC will be considered `standard` from Tuesday March 21 at 00:00 UTC, and
changes the the projects, repositories and accounts will then be implemented.

# Status

This proposal is currently in the `standard` state.

# References

 - Discussion issue: tpo/tpa/team#40946
