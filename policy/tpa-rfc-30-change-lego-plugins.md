---
title: TPA-RFC-30: Changing how lego plugins are used
---

[[_TOC_]]

Summary: this RFC seeks to change the way plugins in lektor projects are structured and symlinked.

# Background

Currently, new and existing lektor projects consume and install lektor plugins from lego by symlinking `packages -> lego/packages/`. As we add new plugins to lego, this means that every single lektor project will install and use the plugin. This isn't much of an issue for well-behaved plugins that require a lektor build flag to activate. However, many smaller plugins (and some larger ones) don't use a build flag at all; for instance @kez wrote the [lektor-md-tag](https://gitlab.torproject.org/tpo/web/lego/-/tree/main/packages/lektor-md-tag) plugin that doesn't use a build flag, and the lektor-i18n-plugin we use has caused issues by not using a build flag tpo/web/team#16

# Proposal

The proposed change to how lego packages are used is not to symlink the entire `packages -> lego/packages/`, but to create a `packages/` directory in each lektor project, and symlink individual plugins i.e. `packages/envvars -> ../lego/packages/envvars/` and `packages/i18n -> ../lego/packages/envvars/`.

## Goals

- All existing lektor sites change the way they symlink packages
- All existing lektor sites *only* symlink what they need
- The tpo/web/template repository doesn't symlink any packages, and the README explicitly states how to use packages
- This change is documented in the tpo/web/documentation wiki

## Scope

This RFC only affects how plugins are linked within a project. New plugins, and how assets are linked are out of scope for this RFC.

## Affected users

The only users affected by this will be the few adding new plugins.

### Examples or Personas

Examples:

 * Johnny WebDeveloper: Johnny wants to add a new plugin to every lego site. Johnny will have to add the plugin to lego, and then update lego and symlink the plugin for each lektor site. Without this RFC, Johnny would've had to do the same thing, just without the last symlink step.

 * Bonny WebDeveloper (no relation): Bonny wants to add a new plugin to a single site. Bonny may add this plugin to lego and then only symlink it for one repo, or Bonny may decide to add it directly to the repo without touching lego. Without this RFC Bonny wouldn't be able to add it to just one repo, and would need to enable it for all sites.

# Alternatives considered

Not applicable.

# Costs

None.

# Approval

This RFC will need approval form TPA and web teams before being Accepted.

# Deadline

This proposal will be brought up to tpa-team@ and tor-www@ with the deadline being set for 1 week: June 8 2022

# Status

This proposal is currently in the `standard` state.

# References

Discussion thread: tpo/web/team#16
