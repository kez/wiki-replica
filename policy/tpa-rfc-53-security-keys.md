---
title: TPA-RFC-53: Everyone gets a security key!
---

[[_TOC_]]

Summary: provide staff and core contributors with cryptographic
security keys at the next Tor meeting.

# Background

The Tor Project has been slowly adopting two-factor authentication
(2FA) in many of our services. This, however, has been done
haphazardly so far; there's no universal policy of whether or not 2FA
should be used or how it should be implemented.

In particular, in some cases 2FA means phone-based (or "TOTP")
authentication systems (like Google Authenticator). While those are
better than nothing, they are somewhat not as secure as the
alternative, which is to use a piece of hardware dedicated to
cryptographic operations. Furthermore, TOTP systems are prone to
social engineering attacks.

This matters because some high profile organizations like ours were
compromised by hacking into key people's accounts and destroying
critical data or introducing vulnerabilities in their software. Those
organisations had 2FA enabled, but attackers were able to bypass that
security by hijacking their phones or flooding it with notifications,
which is why having a cryptographic token like a Yubikey is important.

In addition, we do not have any policy regarding secrets storage: in
theory, someone could currently store their OpenPGP or SSH keys,
on-disk, in clear-text, and wouldn't be in breach of an official,
written down policy.

Finally, even if we would be to develop such a policy, we don't
currently provide the tools or training to our staff and volunteers to
actually implement this properly.

## Survey results

In March 2023, a survey was conducted on tor-internal to probe
people's interest in the matter. Everyone who didn't already have a
"Yubikey" wanted one, which confirmed the theory this is something that
strongly interests people.

The survey also showed people are interested in the devices not just
for 2FA but *also* for private key storage, including SSH (72%) and
OpenPGP (62%!). There was also some interest in donating keys to
volunteers (26%).

# Proposal

Ensure that everyone who wants to has access to industry-standard,
high quality cryptographic tokens that allow for web-based 2FA
(through FIDO2) but also SSH and OpenPGP operations.

Technically, this consists of getting a sponsorship from Yubico to get
a batch of Yubikeys shipped at the coming Tor meeting. Those will
consist of normal-sized [Yubikey 5 NFC][] (USB-A) and [Yubikey 5C
NFC][] (USB-C) keys.

We will also provide basic training on how to use the keys,
particularly how to onboard the keys on Nextcloud, Discourse, and
GitLab, alongside recovery code handling. 

An optional discussion will also be held around cryptographic key
storage and operations with SSH and OpenPGP. There are significant
pitfalls in moving cryptographic keys to those tokens that should be
taken into account (what to do in case of loss, etc), particularly for
encryption keys.

[Yubikey 5 NFC]: https://www.yubico.com/product/yubikey-5-nfc/
[Yubikey 5C NFC]: https://www.yubico.com/product/yubikey-5c-nfc/

## Why FIDO2?

Why do we propose FIDO2 instead of TOTP or other existing standards?
FIDO2 has stronger promises regarding phishing protection, as secrets
are cryptographically bound to the domain name of the site in
use. 

This means that an attacker that would manage to coerce a user into
logging in to a fraudulent site would still not be able to extract the
proper second factor from the FIDO2 token, something that solutions
like TOTP (Google Authenticator, etc) do not provide.

## Why now?

We're meeting in person! This seems like a great moment to physically
transmit security sensitive hardware, but also and especially train
people on how to use them.

Also, GitHub started enforcing 2FA for some developers in a rollout
starting [from March 2023][].

[from March 2023]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/41083

# Affected users

This affects all core contributors. Not everyone will be *forced* to
use those tokens, but everyone interested in improving their security
and that of the organisation are encouraged to join the
program. People in key positions with privileged access are strongly
encouraged to adopt those technologies in one form or another.

# Alternatives considered

## General security policy

The idea here is not to force anything on the organisation: there is a
separate discussion to establish a security policy in [TPA-RFC-18][].

[TPA-RFC-18]: policy/tpa-rfc-18-security-policy

## Nitrokey, Solokey, Titan key and other devices

There are a number of other cryptographic tokens out there. Back in
2017, anarcat produced a [review of various tokens][]. The Nitrokey
was interested, but was found to be too bulky and less sturdy than the
Yubikey.

Solokey was also considered but is not quite ready for prime time
yet.

Google's Titan key was also an option, but the contact at this point
was made with Yubico people.

That said, contributors are free to use the tokens of their choice.

[review of various tokens]: https://anarc.at/blog/2017-10-26-comparison-cryptographic-keycards

## Getting rid of passwords

[Passkeys][] are an emerging standard that goes beyond what we are
planning here. To quote the website, they are "a replacement for
passwords that provide faster, easier, and more secure sign-ins to
websites and apps across a user’s devices."

We are not getting rid of passwords, at least not yet. While passwords
are indeed a problem, we're taking a more short-term approach of "harm
reduction" by reducing the attack surface using technologies we know
and understand *now*. One out of six people in the survey already have
Yubikeys so the inside knowledge for that technology is well
established, we are just getting tools in people's hands right now.

[Passkeys]: https://fidoalliance.org/passkeys/

## Single sign on

The elephant in the room in this proposal is how all our
authentication systems are disconnected. It's something that should
probably be fixed in time, but is not covered by this proposal.

## Individual orders

We are getting lots of keys at once because we hope to bypass possible
[interdiction][] as we hope to get the keys in person. While it is
possible for Yubico itself to be compromised, the theory is that going
directly to them does not raise the risk profile, while removing an
attack vector.

That said, contributors are free to get keys on their own, if they
think they have a more secure way to get those tokens.

[interdiction]: https://en.wikipedia.org/wiki/Interdiction

# Costs

Staff time, hardware donated by Yubico.

# Approval

Core contributors should approve.

# Deadline

In one week I will finalize the process with Yubico unless an
objection is raised on tor-internal.

# Status

This proposal is currently in the `obsolete` state.

# References

Comments welcome in the [discussion ticket][].

[discussion ticket]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/41083
