Agenda
======

 * Introductions
 * Pointers for new peopple
   * https://gitlab.torproject.org/anarcat/wikitest/-/wikis/
   * nagios https://nagios.torproject.org/cgi-bin/icinga/status.cgi?allunhandledproblems&sortobject=services
   * [open tickets](https://web.archive.org/web/20190516173003/https://trac.torproject.org/projects/tor/query?status=accepted&status=assigned&status=needs_information&status=needs_review&status=needs_revision&status=new&status=reopened&component=Internal+Services%2FTor+Sysadmin+Team&col=id&col=summary&col=component&col=status&col=type&col=priority&col=milestone&report=44&desc=1&order=id)
   * git repos
     * https://gitweb.torproject.org/admin
     * ssh://pauli.torproject.org/srv/puppet.torproject.org/git/tor-puppet
 * What we've been working on in Feb
 * What's up for March
 * Any other business
   * the cymru hw
 * Onboarding tasks
   * trying to answer a gazillion questions from anarcat
 * Next meeting is April 1, 16:00 UTC
 * Ending meeting no later than 17:00 UTC

Report
======

Posted on the [tor-project mailing list](https://lists.torproject.org/pipermail/tor-project/2019-March/002246.html).

What happened in feb
--------------------

 * roger: would like prios from team and people and project manage it
 * ln5: upgrading stuff, gitlab setup, civicrm, ticketing
 * hiro: website redesign, prometheus test (munin replacement)
 * weasel: FDE on hetzner hosts, maybe with mandos
 * qbi: website translation, trac fixing

Anarcat Q&A
-----------

### Main pain points

 1. trac gets overwhelmed
 2. cymru doesn't do tech support well
 3. nobody knows when services stop working

### Machine locations

  1. cymru (one machine with multiple and one VM in their cluster)
  2. hetzner
  3. greenhost
  4. linus' org (sunet.se)
