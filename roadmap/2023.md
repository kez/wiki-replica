This page documents the mid-term plan for TPA in the year 2022.

Previous [roadmaps](roadmap) were done in a quarterly and yearly basis, but
in 2022, we used the [OKR](https://en.wikipedia.org/wiki/OKR) system instead. This was not done again
this year and we have a simpler set of milestones we'll try to achieve
during the year.

The roadmap is still ambitious, possibly too much so, and like the
OKRs, it's unlikely we complete them all. But we agree those are
things we want to do in 2023, given time.

Those are the big projects for 2023:

# sysadmin

 * do the [bookworm upgrades](https://gitlab.torproject.org/groups/tpo/tpa/-/milestones/6), this includes:
   * bullseye upgrades (!)
   * puppet server 7
   * puppet agent 7
   * plan would be:
     * Q1-Q2: deploy new machines with bookworm
     * Q1-Q4: upgrade existing machines to bookworm
   * **Status**: 50% complete. Scheduled for 2024 Q1/Q2.
 * email services improvements ([TPA-RFC-45](https://gitlab.torproject.org/tpo/tpa/team/-/issues/41009), milestone to create), includes:
   * upgrade Schleuder and Mailman 2: not done yet, hopefully 2024 Q2
   * self-hosting Discourse: done!
   * hosting/improving email service in general: hasn't moved forward,
     hopefully planned in q2 2024
 * [complete the cymru migration](https://gitlab.torproject.org/groups/tpo/tpa/-/milestones/2): done! working well, no
   performance issues, more services hosted there than we started,
   still have capacity 🎉 but took more time to deploy than expected
 * [old service retirements](https://gitlab.torproject.org/groups/tpo/tpa/-/milestones/7)
   * retire gitolite/gitweb (e.g. execute [TPA-RFC-36](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-36-gitolite-gitweb-retirement), now [its
     own milestone](https://gitlab.torproject.org/groups/tpo/tpa/-/milestones/11))" did progress a bit, most people have moved
     off, no push to any repository since announcement. Probably will
     lock-down in the next month or two, hope to be retired in Q3 2024
   * retire SVN (e.g. execute TPA-RFC-11): no progress. plan adopted
     in Costa Rica to have a new Nextcloud, but reconsidered at the
     ops meeting (nc will not work as an alternative because of [major
     issues with collaborative editing](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/nextcloud#a-few-gotchas-with-collaborative-editing)), need to go back to the
     drawing board
   * monitoring system overhaul ([TPA-RFC-33](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40755)): rough consensus
     in place, proposal/eval of work to be done
 * [deploy a Puppet CI](https://gitlab.torproject.org/groups/tpo/tpa/-/milestones/8#tab-issues): no work done

We were [overwhelmed in late 2023](https://gitlab.torproject.org/tpo/tpa/team/-/issues/41411) which delayed many projects,
particularly the mail services overhaul.

# web

The following was accomplished:

 * transifex / weblate migration
 * blog improvement
 * developer portal
 * user stories

# per quarter reviews

Actual quarterly allocations are managed in a [Nextcloud spreadsheet](https://nc.torproject.net/apps/onlyoffice/458448?).
